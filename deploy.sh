#!/bin/sh

ansible_exists() {
  which ansible &>/dev/null
}

ansible_install() {
  echo 'Installing Ansible'
  sudo apt-get install -y epel-release
  sudo apt-get install -y '@Development tools'
  sudo apt-get install -y openssl-devel libffi-devel python python-devel python-pip libselinux-python
  sudo pip install paramiko==1.16.0
  sudo pip install ansible
}

ansible_deploy() {
  if [ -f "./vault/$1.key" ]; then
    ansible-playbook --inventory-file=inventory/${1}.ini --vault-password-file=vault/${1}.key main.yml  -K -v
  else
    ansible-playbook --inventory-file=inventory/${1}.ini main.yml -K
  fi
}

export PYTHONUNBUFFERED=1
export ANSIBLE_FORCE_COLOR=true
export ANSIBLE_HOST_KEY_CHECKING=false
export ANSIBLE_SSH_ARGS='-o UserKnownHostsFile=/dev/null -o ControlMaster=auto -o ControlPersist=60s'

( ansible_exists || ansible_install ) && ansible_deploy $@
