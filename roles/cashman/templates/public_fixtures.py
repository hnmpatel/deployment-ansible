from django.conf import settings
from django.contrib.auth.models import User

from knox.models import AuthToken

from customers.models import Client

Client.objects.create(name='public', schema_name='public', domain_url='{{ django.domain }}')
Client.objects.create(name='absperf', schema_name='absperf', domain_url='absperf.{{ django.domain }}')

user = User.objects.create_superuser(
    '{{ django.superuser.username }}',
    '{{ django.superuser.email }}',
    '{{ django.superuser.password }}',
    first_name='Site', last_name='Administrator'
)
AuthToken.objects.create(user=user)
