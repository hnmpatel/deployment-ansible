from django.contrib.auth.models import User
from knox.models import AuthToken
from workflow.models import Agent

user = User.objects.create_superuser(
  '{{ django.superuser.username }}',
  '{{ django.superuser.email }}',
  '{{ django.superuser.password }}',
  first_name='Site', last_name='Administrator'
)

AuthToken.objects.create(user=user)

Agent.objects.create(username='ea-agent', is_active=True, queues=[ {'name': 'EATasks', 'queue': 'ea-tasks', 'exchange': 'ea-tasks', 'routing_key': 'ea-tasks'}, ])
